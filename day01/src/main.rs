use std::collections::HashSet;
use std::fs::File;
use std::io::Read;

fn main() {

	let mut file = File::open("input")
		.expect("can't open file");

	let mut seen_freqs: HashSet<i32> = HashSet::new();
	
	let mut result = 0;

	seen_freqs.insert(0);


	let mut contents = String::new();
 	file.read_to_string(&mut contents)
		.expect("derp read file");

    for i in contents.lines().cycle() {
    //for i in contents.lines() {
	    let val = i.parse::<i32>().unwrap();
		result = result + val;

		if seen_freqs.contains(&result) {
			println!("Repeated key {}", result);
			break;
		}

		seen_freqs.insert(result);
	    println!("{}", result);
	}


}
