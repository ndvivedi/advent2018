use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::Read;

fn main() {

	//part_01();
	part_02();
	//part_02_f();
}

fn part_02_f() {

	let mut contents = String::new();

	let mut file = File::open("input")
		.expect("can't open file");

 	file.read_to_string(&mut contents)
		.expect("can't read file");

	let mut seen = HashSet::new();

	for input in contents.lines() {

		let len = input.len();
		
		for i in 0..len {
			let start = &input[0..i];
			let end = &input[(i+1)..len];

			let mut s = String::new();
			s.push_str(start);
			s.push('_');
			s.push_str(end);

			if seen.contains(&s) {
				println!("done! {}", s);
				return;
			}
			seen.insert(s);
		}
	}
}

fn part_02() {
	let _test_data = [
		"abcde",
		"fghij",
		"klmno",
		"pqrst",
		"fguij",
		"axcye",
		"wvxyz",
	];

	let mut file = File::open("input")
		.expect("can't open file");

	let mut contents = String::new();
 	file.read_to_string(&mut contents)
		.expect("can't read file");

	for input1 in contents.lines() {
		for input2 in contents.lines() {
		    let (n_diffs, same_chars) = number_char_diffs(input1, input2);
			if n_diffs == 1 {
				println!("{} {} {}", input1, input2, same_chars);
			}
		}
	}
}

fn number_char_diffs(a: &str, b: &str) -> (u32, String) {
	assert!(a.len() == b.len());

	let mut diffs = 0;
	let mut s = String::new();

	for it in a.chars().zip(b.chars()) {
		let (aa, bb) = it;
		if aa != bb {
			diffs += 1;
		}
		else {
			s.push(aa);
		}
	}

	(diffs, s)
}

fn part_01() {

	let mut file = File::open("input")
		.expect("can't open file");

	let mut contents = String::new();
 	file.read_to_string(&mut contents)
		.expect("can't read file");

	let mut twos = 0;
	let mut threes = 0;

	for line in contents.lines() {
		let (two, three) = two_or_three(&line);

		if two { twos += 1; }
		if three { threes += 1; }
	}

	println!("twos: {}, threes: {}, checksum {}", twos, threes, twos*threes);

}

fn string_char_count(input: &str) -> HashMap<char, u32> {

	let mut char_counts = HashMap::new();

	char_counts.insert('H', 1);

	for c in input.chars() {
		let char_count = char_counts.entry(c).or_insert(0);
		*char_count += 1;
	}

	char_counts
}

fn two_or_three(input: &str) -> (bool, bool) {

	let counts = string_char_count(input);

	let mut has_two = false;
	let mut has_three = false;

	for (_k, v) in counts {
	    if v == 2 {
			has_two = true;
		}
	    if v == 3 {
			has_three = true;
		}
	}

	(has_two, has_three)
}
