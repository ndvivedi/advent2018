use std::collections::HashMap;
use std::collections::HashSet;
use std::io::prelude::*;

fn main() {

    let mut contents = String::new();
    std::io::stdin().read_to_string(&mut contents)
        .expect("error reading stdin");

    day03(&contents);
}

type Claim<'a> = (&'a str, u32, u32, u32, u32);

fn day03(input: &str) {

    let mut coords: HashMap<(u32, u32), Vec<&str>> = HashMap::new();
    let mut claim_ids: HashSet<&str> = HashSet::new();

    for line in input.lines() {
        let (id, x, y, sx, sy) = parse_line(line);

        for i in x..(x+sx) {
            for j in y..(y+sy) {
                let v = coords.entry((i, j)).or_insert(Vec::new());
                v.push(id);
            }
        }

        claim_ids.insert(id);
    }

    let mut overlap_count = 0;

    for (_, ids) in coords {
        if ids.len() > 1 {
            overlap_count += 1;
            for id in ids { claim_ids.remove(&id); }
        }
    }

    println!("found {} overlaps", overlap_count);
    for id in claim_ids { println!("{}", id)}
}

//just use regex next time..
fn parse_line(input: &str) -> Claim {
    let parts: Vec<&str> = input.split(" ").collect();

    let id = parts[0];

    let starting_coords: Vec<&str> = parts[2].split(',').collect();
    let x = starting_coords[0].parse::<u32>().unwrap();
    let y = starting_coords[1][0..(starting_coords[1].len()-1)].parse::<u32>().unwrap();

    let size_parts: Vec<&str> = parts[3].split('x').collect();
    let size_x = size_parts[0].parse::<u32>().unwrap();
    let size_y = size_parts[1].parse::<u32>().unwrap();
    
    (&id[1..], x, y, size_x, size_y)
}