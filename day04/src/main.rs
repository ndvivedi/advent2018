extern crate regex;

use std::collections::HashMap;
use std::io::Read;
use std::str::Lines;
use std::str::FromStr;

use regex::Regex;

fn main() {

	let mut contents = String::new();
 	std::io::stdin().read_to_string(&mut contents)
		.expect("can't read file");

	day04(contents.lines())
}

fn day04(lines: Lines) {

	let mut logs: Vec<LogEvent> = lines
		.map(|l| l.parse::<LogEvent>().unwrap())
		.collect();

	logs.sort();

	let mut shifts: Vec<Shift> = vec![];

	//state for the loop over all logs
	let mut on_duty_guard = None;
	let mut sleep_start = None;

	let mut sleep_ranges = vec![];

	for event in logs {

		if let EventType::ShiftStart { id } = event.event_type {
			//if there is a current guard push the sleep ranges onto the shifts vec
			if let Some(guard_id) = on_duty_guard {
				shifts.push(Shift { id: guard_id, sleep_ranges: sleep_ranges.clone() });
				sleep_ranges = vec![];
			}
			on_duty_guard = Some(id);
		}

		if event.event_type == EventType::FallAsleep {
			sleep_start = Some(event.timestamp.clone())
		}

		if event.event_type == EventType::WakeUp {
			if let Some(cur_sleep_start) = sleep_start {
				sleep_ranges.push((cur_sleep_start.clone(), event.timestamp.clone()));
				sleep_start = None;
			}
		}
	}

	if let Some(guard_id) = on_duty_guard {
		shifts.push(Shift { id: guard_id, sleep_ranges: sleep_ranges.clone() });
	}

	let mut shifts_by_guard: HashMap<u32, Vec<Shift>> = HashMap::new();

	for shift in shifts {
		let guards_shifts = shifts_by_guard.entry(shift.id).or_insert(Vec::new());
		guards_shifts.push(shift);
	}

	part01(&shifts_by_guard);
	part02(&shifts_by_guard);
}


fn part01(shifts_by_guard: &HashMap<u32, Vec<Shift>>) {

	let guards_total_sleep: HashMap<_, _> = shifts_by_guard.iter()
		.map(|(id, shifts)| (id, sleep_len(shifts)))
		.collect();

	let sleepiest_guard = guards_total_sleep.iter().max_by_key(|a| a.1).unwrap().0;

	let (highest_min, _freq) = highest_minute_for_shift(&shifts_by_guard.get(sleepiest_guard).unwrap());

	println!("sleepiest guard {} most freq sleep minute {}: {}", sleepiest_guard, highest_min, *sleepiest_guard * highest_min);
}

fn part02(shifts_by_guard: &HashMap<u32, Vec<Shift>>) {

	let (id, (minute, freq)) = shifts_by_guard.iter()
		.map(|(id, shifts)| (id, highest_minute_for_shift(&shifts)))
		.max_by_key(|x| (x.1).1)
		.unwrap();


	println!("guard {} slept {} times at minute {}: {}", id, freq, minute, id*minute);
}

fn sleep_len(shifts: &Vec<Shift>) -> u32 {
	shifts.iter()
		.map(|shift| sum_sleeps(&shift.sleep_ranges))
		.sum()
}

fn sum_sleeps(sleeps: &Vec<(Timestamp, Timestamp)>) -> u32 {
	sleeps.iter()
		.map(|single_range| single_range.1.minute - single_range.0.minute)
		.sum()
}

fn highest_minute_for_shift(shifts: &Vec<Shift>) -> (u32, u32) {
	let ranges: Vec<(Timestamp, Timestamp)> = shifts.iter()
		.flat_map(|shift| shift.sleep_ranges.clone())
		.collect();

	highest_minute(&ranges)
}
fn highest_minute(sleeps: &Vec<(Timestamp, Timestamp)>) -> (u32, u32) {

	let mut minutes: HashMap<u32, u32> = HashMap::new();

	for (start, end) in sleeps {
		for min in start.minute..end.minute {
			*minutes.entry(min).or_default() += 1;
		}
	}

	match minutes.iter().max_by_key(|a| a.1) {
		Some((a, b)) => (*a, *b),
		None => (0, 0),
	}
}

#[derive(Debug)]
struct Shift {
	id: u32,
	sleep_ranges: Vec<(Timestamp, Timestamp)>
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone)]
struct Timestamp {
	year: u32,
    month: u32,
    day: u32,
    hour: u32,
    minute: u32,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
struct LogEvent {
	timestamp: Timestamp,
	event_type: EventType,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
enum EventType {
	ShiftStart { id: u32 },
	FallAsleep, 
	WakeUp,
}

// [1518-11-01 00:00] Guard #10 begins shift
// [1518-11-01 00:05] falls asleep
// [1518-11-01 00:25] wakes up
// [1518-11-01 00:30] falls asleep
// [1518-11-01 00:55] wakes up
// [1518-11-01 23:58] Guard #99 begins shift
// [1518-11-02 00:40] falls asleep
// [1518-11-02 00:50] wakes up
// [1518-11-03 00:05] Guard #10 begins shift
// [1518-11-03 00:24] falls asleep
// [1518-11-03 00:29] wakes up
// [1518-11-04 00:02] Guard #99 begins shift
// [1518-11-04 00:36] falls asleep
// [1518-11-04 00:46] wakes up
// [1518-11-05 00:03] Guard #99 begins shift
// [1518-11-05 00:45] falls asleep
// [1518-11-05 00:55] wakes up

impl FromStr for LogEvent {
	type Err = ();


	fn from_str(s: &str) -> Result<Self, ()> {

		let pattern: Regex = Regex::new(r"(?x)
					\[
					(?P<year>[0-9]{4})-(?P<month>[0-9]{2})-(?P<day>[0-9]{2})
					\s+
					(?P<hour>[0-9]{2}):(?P<minute>[0-9]{2})
					\]
					\s+
					(?:Guard\ \#(?P<guard_id>[0-9]+)|(?P<msg>.+))
		").unwrap();

		let caps = pattern.captures(s).unwrap();

        let timestamp = Timestamp {
            year: caps["year"].parse().unwrap(),
            month: caps["month"].parse().unwrap(),
            day: caps["day"].parse().unwrap(),
            hour: caps["hour"].parse().unwrap(),
            minute: caps["minute"].parse().unwrap(),
        };

        let event_type =
            if let Some(m) = caps.name("guard_id") {
                EventType::ShiftStart { id: m.as_str().parse().unwrap() }
            } else if &caps["msg"] == "falls asleep" {
                EventType::FallAsleep
            } else if &caps["msg"] == "wakes up" {
                EventType::WakeUp
            } else {
                return Err(())
            };

		Ok(LogEvent { timestamp: timestamp, event_type: event_type })
	}
}
