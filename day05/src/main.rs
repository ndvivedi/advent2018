use std::io::Read;

fn main() {

	let mut contents = String::new();
 	std::io::stdin().read_to_string(&mut contents)
		.expect("can't read file");

    let result = run(&contents);
    println!("part1 {}", result.len());

    let best = part2(&contents);
    println!("part2 {}", best.len());
}

fn part2(input: &str) -> String {

    let alphabet = "abcdefghijklmnopqrstuvwxyz";
    let mut best = input.to_string();

    for c in alphabet.chars() {
        let filtered_input: String = input.chars().filter(|x| x.to_ascii_lowercase() != c).collect();
        let output = run(&filtered_input);

        if output.len() < best.len() {
            best = output
        }
    }

    best
}

fn run(input: &str) -> String {

    let mut out: Vec<char> = vec![];

    for c in input.chars() {
        //needed to pop rather than get ref via out.last() to satisfy borrow checker
        if let Some(b) = out.pop() {
            //if we shouldn't reduce then put the pop'd char and new char back into output
            if !should_reduce(c, b) {
                out.push(b);
                out.push(c);
            }
        }
        else {
            out.push(c);
        }
    }

    out.iter().collect()
}

fn should_reduce(a: char, b: char) -> bool {
    a != b && (a.to_ascii_lowercase() == b.to_ascii_lowercase())
}
