use std::io::Read;
use std::collections::HashSet;

fn main() {

	let mut contents = String::new();
 	std::io::stdin().read_to_string(&mut contents)
		.expect("can't read file");

	let (coords, xmax, ymax) = read_coords(&contents);

	part01(&coords, xmax, ymax);
	part02(&coords, xmax, ymax);
}

fn read_coords(input: &str) -> (Vec<Coord>, u32, u32) {

	let coords: Vec<_> = input.lines().map(|s| parse(s)).collect();	

	let mut xmax = coords[0].0;
	let mut ymax = coords[0].1;

	for (x, y) in coords.iter() {
		xmax = std::cmp::max(xmax, *x);
		ymax = std::cmp::max(ymax, *y);
	}

	(coords, xmax as u32, ymax as u32)
}

fn part01(coords: &Vec<Coord>, xmax: u32, ymax: u32) {

	let width = (xmax + 2) as usize;
	let height = (ymax + 2) as usize;

//grid stuff stolen from: https://stackoverflow.com/questions/13212212/creating-two-dimensional-arrays-in-rust
// the dynamic 2-d vec answer
	let mut grid_raw = vec![0; width * height];
    let mut grid_base: Vec<_> = grid_raw.as_mut_slice().chunks_mut(width).collect();
    let grid: &mut [&mut [_]] = grid_base.as_mut_slice();

	let mut inf = HashSet::new();

	for i in 0..width {
		for j in 0..height {

			let mut distances: Vec<_> = coords.iter().enumerate()
				.map(|(n, coord)| (n, distance(coord, &(i as i32, j as i32))))
				.collect();

			distances.sort_by_key(|x| x.1);

			//if the first two distances are the same then use -1
			let d = if distances[0].1 == distances[1].1 {
				-1 
			}
			else {
				distances[0].0 as i32
			};

			grid[j][i] = d;

			//consider anything at the edges to be "infinite"
			if i == 0 || i == (width-1) {
				inf.insert(d);
			}
			if j == 0 || j == (height-1) {
				inf.insert(d);
			}
		}
	}


//so gross everything below here

    //count frequency of closeness
	let mut counts: Vec<u32> = vec![0; coords.len()];
	for x in grid {
		for y in x.iter() {
			if *y != -1 && !inf.contains(y) {
				counts[*y as usize] += 1;
			}
		}
	}

    //get the index of largest element
	let mut size = counts.iter().enumerate().collect::<Vec<_>>();
	size.sort_by_key(|x| x.1);
	let biggest = size.pop().unwrap().1;

	println!("part1 {}", biggest);
}

fn part02(coords: &Vec<Coord>, xmax: u32, ymax: u32) {
	let mut close_spots = 0;

	for i in 0..xmax {
		for j in 0..ymax {

			let sum_of_dists: i32 = coords.iter()
				.map(|coord| distance(coord, &(i as i32, j as i32)))
				.sum();

			if sum_of_dists < 10000 {
				close_spots += 1;
			}
		}
	}

	println!("part2 {}", close_spots);
}

fn parse(line: &str) -> Coord {
	let parts: Vec<_> = line.split(", ").collect();
	let x: i32 = parts[0].parse().unwrap();
	let y: i32 = parts[1].parse().unwrap();
	(x, y)
}

type Coord = (i32, i32);

fn distance(a: &Coord, b: &Coord) -> i32 {
	let (x1, y1) = a;
	let (x2, y2) = b;
	(x2-x1).abs() + (y2-y1).abs()
}
