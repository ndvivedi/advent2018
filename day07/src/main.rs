use std::collections::BTreeSet;
use std::collections::HashMap;
use std::io::Read;

fn main() {
    let mut contents = String::new();
    std::io::stdin()
        .read_to_string(&mut contents)
        .expect("can't read file");

    let edges = read_edges(&contents);

    sort(edges.clone());
    run_sim(edges.clone());
}

fn sort(edges: Vec<(char, char)>) {
    let cloned_edges = edges.clone();
    let mut verts: BTreeSet<_> = cloned_edges.iter().flat_map(|(a, b)| vec![a, b]).collect();

    let mut edges_by_vert_out = HashMap::new();
    let mut num_edges_by_vert_in: HashMap<char, u32> = HashMap::new();
    for (a, b) in edges {
        verts.remove(&b);
        edges_by_vert_out
            .entry(a)
            .or_insert(BTreeSet::new())
            .insert(b);
        *num_edges_by_vert_in.entry(b).or_default() += 1;
    }

    let mut output = vec![];

    while !verts.is_empty() {
        //if i don't use cloned() here this is a mutable ref and the world ends
        let node = verts.iter().cloned().next().unwrap();

        output.push(*node);

        if let Some(next_nodes) = edges_by_vert_out.get(node) {
            for b in next_nodes.iter() {
                let in_edges = num_edges_by_vert_in.get(b);
                if let Some(c) = in_edges {
                    if *c == 1 {
                        verts.insert(b);
                    }
                    num_edges_by_vert_in.insert(*b, *c - 1);
                }
            }
        }
        verts.remove(node);
    }

    let output_str: String = output.iter().collect();
    println!("{}", output_str);
}

fn run_sim(edges: Vec<(char, char)>) {
    let cloned_edges = edges.clone();
    let mut verts: BTreeSet<_> = cloned_edges.iter().flat_map(|(a, b)| vec![a, b]).collect();

    let mut edges_by_vert_out = HashMap::new();
    let mut num_edges_by_vert_in: HashMap<char, u32> = HashMap::new();
    for (a, b) in edges {
        verts.remove(&b);
        edges_by_vert_out
            .entry(a)
            .or_insert(BTreeSet::new())
            .insert(b);
        *num_edges_by_vert_in.entry(b).or_default() += 1;
    }

    /*
        Represent the work as two vectors.  As work becomes available
        we just push it onto the shorter of the two vectors.  When we push
        work onto a vector we will just push n chars for the size of the work,
        for example if C becomes available we push three C's onto the appropriate
        vector.
    */
    //let mut work = (vec![], vec![]);

    // while !verts.is_empty() {
    //     //if i don't use cloned() here this is a mutable ref and the world ends
    //     let node = verts.iter().next().unwrap();

    //     output.push(*node);

    //     if let Some(next_nodes) = edges_by_vert_out.get(node) {
    //         for b in next_nodes.iter() {
    //             let in_edges = num_edges_by_vert_in.get(b);
    //             if let Some(c) = in_edges {
    //                 if *c == 1 {
    //                     verts.insert(b);
    //                 }
    //                 num_edges_by_vert_in.insert(*b, *c - 1);
    //             }
    //         }
    //     }
    //     verts.remove(node);
    //}
}

//Step D must be finished before step E can begin.
fn read_edges(lines: &str) -> Vec<(char, char)> {
    lines
        .lines()
        .map(|s| s.chars())
        .map(|mut s| (s.nth(5).unwrap(), s.nth(30).unwrap()))
        .collect()
}
